﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;

namespace WhaleDevelop.TCPServer.SMTOA
{
    ///<summary> Класс, обрабатывающий соединение с проектом SM/TOA</summary>
    [CreateAssetMenu(fileName = "SMTOAConnectionHandler", menuName = "ConnectionHandlers/SMTOAConnectionHandler")]
    public class SMTOAConnectionHandler : ServerConnectionHandler
    {
        #region fields and props

        [Header("Список обработчиков сообщений")]
        [SerializeField] private List<MessagesHandler> messagesHandlers = new List<MessagesHandler>();
        [Space(20)]
        [Tooltip("Сообщения, уведомление о котором не показывается в консоли")]
        [SerializeField] private List<MessageCode> messagesDebugIngoreList = new List<MessageCode>();
        
        private Dictionary<MessageCode, MessagesHandler> handlersFastSearchDictionary = new Dictionary<MessageCode, MessagesHandler>();
      
        private MessagesHandler GetHandlerForMessage(MessageCode messageCode)
        {
            if (handlersFastSearchDictionary.ContainsKey(messageCode))
                return handlersFastSearchDictionary[messageCode];
            else
                return messagesHandlers.Find(handler => handler.messagesCodes.Contains(messageCode));
        }

        #endregion

        public override void OnInit()
        {
            foreach (MessagesHandler messagesHandler in messagesHandlers)
            {
                // Прокидываем ссылку на обработчик соединения
                messagesHandler.Init(this);
                // Создаем словарь связей кодов сообщений - обработчиков для быстрого доступа 
                foreach (MessageCode messageCode in messagesHandler.messagesCodes)
                {
                    if (!handlersFastSearchDictionary.ContainsKey(messageCode))
                        handlersFastSearchDictionary.Add(messageCode, messagesHandler);
                    else
                        Debug.Log("Для сообщения " + messageCode.ToString() + " уже есть обработчик");
                }
            }
       
        }

        public override void OnReceiveMessage(int clientId, string messageString)
        {
            Message message = JsonUtility.FromJson<Message>(messageString);

            if (message != null)
            {
                MessagesHandler messageHandler = GetHandlerForMessage(message.MessageCode);

                DebugMessage(message.MessageCode, messageString, "Receive");

                // Пропускаем сообщения через обработчики, если для них нужны, если нет - просто
                // рассылаем всем клиентам (кроме отправителя)
                if (message.MessageCode != MessageCode.Undefined && messageHandler != null)
                    messageHandler.OnReceiveMessage(clientId, message);
                else
                    server.SendMessageToClients(messageString, clientId);                    
            }
            else
                Debug.Log("Received null message");
        }

        // Для сообщений, у которых рассылаемое сообщение имеет другие данные по отношению к принимаемому
        public void SendMessage(MessageCode messageCode, string messageData, UserRole userRole = UserRole.None)
        {
            Message message = new Message(messageCode, messageData, userRole);
            string messageString = JsonUtility.ToJson(message);
            DebugMessage(messageCode, messageString, "Send");
            SendMessage(messageString);
        }

        // Для сообщений, у которых нужно изменить только обрабатываемые данные
        public void SendMessage(Message receivedMessage, string sendData)
        {
            receivedMessage.messageData = sendData;
            string messageString = JsonUtility.ToJson(receivedMessage);
            DebugMessage(receivedMessage.MessageCode, messageString, "Send");
            SendMessage(messageString);
        }

        public void DisconnectClient(int clientId)
        {
            server?.DisconnectClient(clientId);
        }

        public void HandleAllClientsDisconnection()
        {
            server?.ClearClients();
        }

        public void DebugMessage(MessageCode messageCode, string messageString, string action)
        {
            if (!messagesDebugIngoreList.Contains(messageCode))
                Debug.Log(action + " : " + messageString);
        }
    }
}

