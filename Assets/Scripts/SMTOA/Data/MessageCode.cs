﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WhaleDevelop.TCPServer.SMTOA
{
    public enum MessageCode
    { 
        Undefined,

        // Connection
        ConnectToTCP,
        ChooseRole,
        StartExecutionTCP,
        SyncTimer,
        DisconnectFromTCP,
        SyncTransform
    }
}

