﻿using System;
using System.Collections.Generic;

namespace WhaleDevelop.TCPServer.SMTOA
{
    [Serializable] public class ClientData
    {
        public int clientId;
        public Member memberData;

        public ClientData(int id, Member member)
        {
            this.clientId = id;
            this.memberData = member;
        }
    }
}
