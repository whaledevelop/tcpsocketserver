﻿using System;

namespace WhaleDevelop.TCPServer.SMTOA
{
    [Serializable] public class Message
    {
        public string messageCode;
        public string messageData;
        public string roleCode;
        public string sendTime;

        [NonSerialized] private MessageCode messageCodeEnum;
        public MessageCode MessageCode
        {
            get
            {
                if (messageCodeEnum == MessageCode.Undefined)
                    Enum.TryParse(messageCode, true, out messageCodeEnum);
                return messageCodeEnum;
            }
        }

        [NonSerialized] private UserRole userRoleEnum;
        public UserRole UserRole
        {
            get
            {
                if (userRoleEnum == UserRole.None)
                    Enum.TryParse(roleCode, true, out userRoleEnum);
                return userRoleEnum;
            }
        }

        public Message(MessageCode messageCode, string messageData, UserRole userRole)
        {
            this.messageCode = messageCode.ToString();
            messageCodeEnum = messageCode;
            this.messageData = messageData;
            userRoleEnum = userRole;
            this.roleCode = userRole.ToString();
        }
    }
}
