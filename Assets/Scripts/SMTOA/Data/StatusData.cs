﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhaleDevelop.TCPServer.SMTOA
{
    public enum ReadynessStatus
    {
        Undefined,
        Waiting,
        Ready
    }

    [Serializable]
    public class ReadynessStatusData
    {
        public UserRole userRole;
        public ReadynessStatus status;

        public ReadynessStatusData(UserRole userRole, ReadynessStatus status)
        {
            this.userRole = userRole;
            this.status = status;
        }
    }
}
