﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhaleDevelop.TCPServer.SMTOA
{
    public enum ExecutionMode
    {
        Exam = 0, // Экзаменационный режим - неправильное действие приводит к завершению 
        SelfStudy = 1, // Режим самоподготовки - можно ошибаться
        Animation = 2 // Режим анимации - пользователь не выполняет, а наблюдает за выполнением
    }
}
