﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WhaleDevelop.TCPServer.SMTOA
{
    public enum UserRole : int
    {
        None = 0,
        Chief = 1,          // Начальник расчета
        Substitute = 2,     // Заместитель начальника расчета
        DriverMechanic = 3, // Механик водитель
        ChiefSecond = 4,    // Начальник 2-ого расчета 
        BKP = 5,            // Боевой расчет - условная роль для обозначения взаимодействия вне расчета
    }
}

