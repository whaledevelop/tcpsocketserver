﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WhaleDevelop.TCPServer.SMTOA
{
    [System.Serializable]
    public class Member
    {
        public string roleCode;
        public string roleName;
        public string userCode = "";
        public string userName = "";

        public UserRole RoleCode => !string.IsNullOrEmpty(roleCode)
            ? (UserRole)Enum.Parse(typeof(UserRole), roleCode, true)
            : UserRole.None;
    }

    [Serializable]
    public class ChooseRoleData
    {
        public int cabinetId;
        public Member member;
    }
}

