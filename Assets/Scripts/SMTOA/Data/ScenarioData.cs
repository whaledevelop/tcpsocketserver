﻿using System;
using System.Collections.Generic;

namespace WhaleDevelop.TCPServer.SMTOA
{
    [Serializable] public class ScenarioData
    {
        public string materialType;
        public int scenarioCode;
        public ExecutionMode executionMode;

        public List<UserRole> scenarioRoles;
        public List<ClientData> clients;

        [NonSerialized] private MaterialType materialTypeEnum;
        public MaterialType MaterialType
        {
            get
            {
                if (materialTypeEnum == MaterialType.None)
                    Enum.TryParse(materialType, true, out materialTypeEnum);
                return materialTypeEnum;
            }
        }

        public ScenarioData(MaterialType materialType, int scenarioCode, ExecutionMode executionMode, List<UserRole> scenarioRoles, List<ClientData> clients)
        {
            this.materialType = materialType.ToString();
            this.scenarioCode = scenarioCode;
            this.executionMode = executionMode;
            this.scenarioRoles = scenarioRoles;
            this.clients = clients;
        }
    }
}
