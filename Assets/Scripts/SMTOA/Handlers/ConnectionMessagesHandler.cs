﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace WhaleDevelop.TCPServer.SMTOA
{
    [CreateAssetMenu(fileName = "ConnectionMessagesHandler", menuName = "MessagesHandlers/ConnectionMessagesHandler")]
    public class ConnectionMessagesHandler : MessagesHandler
    {
        [Header("Eсли от клиента будут переданы другие данные, будут заменены")]
        [Tooltip("Тип материала")]
        [NonSerialized] public MaterialType materialType;
        [Tooltip("Номер сценария")]
        [NonSerialized] public int scenarioCode;
        [Tooltip("Режим выполнения")]
        [NonSerialized] public ExecutionMode executionMode;

        [NonSerialized] private List<UserRole> scenarioRoles = new List<UserRole>();
        [NonSerialized] private List<ClientData> clients = new List<ClientData>();
        [NonSerialized] private int readyCount;

        public void OnDisable()
        {
            ClearSessionData();
        }

        public override void OnReceiveMessage(int clientId, Message message)
        {
            switch (message.MessageCode)
            {
                case MessageCode.ConnectToTCP:
                    ScenarioData scenarioData = JsonUtility.FromJson<ScenarioData>(message.messageData);

                    // Если в сообщении указаны данные соединения, то сохраняем их 
                    if (scenarioData != null && scenarioData.MaterialType != MaterialType.None && scenarioData.scenarioCode != 0)
                    {
                        materialType = scenarioData.MaterialType;
                        scenarioCode = scenarioData.scenarioCode;
                        executionMode = scenarioData.executionMode;
                        if (scenarioData.scenarioRoles != null)
                            foreach (UserRole role in scenarioData.scenarioRoles)
                                if (role != UserRole.None && !scenarioRoles.Contains(role))
                                    scenarioRoles.Add(role);
                    }
                    scenarioData = new ScenarioData(materialType, scenarioCode, executionMode, scenarioRoles, clients);
                    //Рассылаем данные соединения (даже если они пустые)
                    connectionHandler.SendMessage(message, JsonUtility.ToJson(scenarioData));
                    break;

                case MessageCode.ChooseRole:

                    ChooseRoleData chooseRoleData = JsonUtility.FromJson<ChooseRoleData>(message.messageData);

                    if (chooseRoleData != null)
                    {
                        if (string.IsNullOrEmpty(chooseRoleData.member.userName))
                        {
                            chooseRoleData.member.userCode = clientId.ToString();
                            chooseRoleData.member.userName = "user" + clientId;
                        }
                        clients.Add(new ClientData(clientId, chooseRoleData.member));

                        connectionHandler.SendMessage(message, JsonUtility.ToJson(chooseRoleData));
                    }
                    break;
                case MessageCode.StartExecutionTCP:
                    readyCount++;
                    ReadynessStatus status = readyCount == scenarioRoles.Count
                        ? ReadynessStatus.Ready
                        : ReadynessStatus.Waiting;
                    ReadynessStatusData statusData = new ReadynessStatusData(message.UserRole, status);
                    connectionHandler.SendMessage(message, JsonUtility.ToJson(statusData));
                    break;

                case MessageCode.DisconnectFromTCP:

                    ClientData disconnectedClient = clients.Find(client => client.clientId == clientId);
                    if (disconnectedClient != null)
                    {
                        clients.Remove(disconnectedClient);
                        if (clients.Count == 0)
                        {
                            ClearSessionData();
                        }
                        else
                        {
                            connectionHandler.DisconnectClient(clientId);
                        }
                    }                   
                    break;
            }
        }

        private void ClearSessionData()
        {
            scenarioRoles = new List<UserRole>();
            clients = new List<ClientData>();
            readyCount = 0;
            connectionHandler?.HandleAllClientsDisconnection();
        }
    }
}
