﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace WhaleDevelop.TCPServer.SMTOA
{
    public class MessagesHandler : ScriptableObject
    {
        [SerializeField] public List<MessageCode> messagesCodes;

        protected SMTOAConnectionHandler connectionHandler;

        public void Init(SMTOAConnectionHandler connectionHandler)
        {
            this.connectionHandler = connectionHandler;
        }

        public virtual void OnReceiveMessage(int clientId, Message message)
        {
            Debug.Log("Receive from " + clientId + " : " + JsonUtility.ToJson(message));
        }
    }
}
