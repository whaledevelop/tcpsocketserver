﻿
using System;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace WhaleDevelop.TCPServer
{
    /// <summary>
    /// Класс обработчик клиентского запроса
    /// </summary>
    public class ServerClient : IDisposable
    {
        private NetworkStream stream;

        public ServerClient(TcpClient tcpClient)
        {
            stream = tcpClient.GetStream();
        }

        #region Получение сообщений
        public async Task<string> ListenToMessagesFromClient()
        {
            // Получаем длину сообщения - они записаны в первых четырех байтах сообщения
            byte[] messageLengthBytes = await ReadFromStreamAsync(4); 
            int messageLength = BitConverter.ToInt32(messageLengthBytes, 0);
            // Дочитываем само сообщения, которое идет после длины
            byte[] messageBytes = await ReadFromStreamAsync(messageLength); // Читаем сообщение указанной длины
            string message = Encoding.UTF8.GetString(messageBytes, 0, messageLength);
            return message;
        }

        // Читает из NetworkStream заданное число байт
        private async Task<byte[]> ReadFromStreamAsync(int messageLength)
        {
            // Изначально создаем массив для сообщения заданной длины
            byte[] messageBytes = new byte[messageLength];
            int readIndex = 0;
            // Проходим по всей длине, считываем сообщение байт за байтом
            while (readIndex < messageLength)
                readIndex += await stream.ReadAsync(messageBytes, readIndex, messageLength - readIndex);
            return messageBytes;
        }

        #endregion

        #region Отправка сообщений

        public void SendMessage(byte[] messageBytes)
        {
            stream.Write(messageBytes, 0, messageBytes.Length);
        }

        public void SendMessage(string message)
        {
            byte[] messageBytes = Server.GetBytesFromString(message);
            stream.Write(messageBytes, 0, messageBytes.Length);
        }

        #endregion

        #region Обрыв соединения

        /// <summary>
        /// Освобождение неуправляемых ресурсов, вызывается автоматически из оператора using
        /// </summary>
        public void Dispose()
        {
            stream.Dispose();
        }

        #endregion
    }
}