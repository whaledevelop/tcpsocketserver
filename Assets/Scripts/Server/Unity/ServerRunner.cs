﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace WhaleDevelop.TCPServer
{
    public class ServerRunner : MonoBehaviour
    {
        #region fields & props

        [Header("Номер порта сервера")]
        [SerializeField] private int port = 8000;

        [Header("Класс-обработчик входящих сообщений. Может быть разный для разных проектов")]
        [SerializeField] private ServerConnectionHandler connectionHandler;

        private Server server;

        private IPAddress ipAddress;

        private IPAddress IPAddress
        {
            get
            {
                if (ipAddress == null)
                {
                    IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
                    List<IPAddress> hostIPAddresses = host.AddressList.ToList();
                    ipAddress = hostIPAddresses.Find(ip => ip.AddressFamily == AddressFamily.InterNetwork);
                }
                return ipAddress;
            }
        }

        #endregion

        #region Monobehaviour
        private void Start()
        {
            if (IPAddress != null && port != 0)
            {
                server = new Server(IPAddress, port);
                if (connectionHandler != null)
                    connectionHandler.Init(server);
                server.Run();
            }
            else
                Debug.LogError("Не указан IP или порт");
        }
        private void OnDestroy()
        {
            if (server != null && server.IsRunning)
                server.Close();
        }

        #endregion
    }
}
