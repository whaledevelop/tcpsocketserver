﻿using System;
using UnityEngine;

namespace WhaleDevelop.TCPServer
{
    ///<summary> Базовый класс, обрабатывающий соединение с сервером</summary>
    [CreateAssetMenu(fileName = "ConnectionHandlerExample", menuName = "ConnectionHandlers/Example")]
    public class ServerConnectionHandler : ScriptableObject
    {
        protected Server server;

        #region Инициализация/закрытие

        public void Init(Server server)
        {
            server.onRun += OnServerRun;
            server.onClose += OnServerClose;
            server.onClientConnected += OnClientConnected;
            server.onClientDisconnected += OnClientDisconnected;
            server.onReceiveMessage += OnReceiveMessage;
            this.server = server;
            OnInit();
        }

        public virtual void OnInit(){}

        #endregion

        #region Методы жизненного цикла сервера

        public virtual void OnServerRun(string ip, int port)
        {
            Debug.Log("Запущен сервер на " + ip + ":" + port);
        }

        public virtual void OnServerClose()
        {
            Debug.Log("Закрытие сервера");
        }

        public virtual void OnClientConnected(int clientId)
        {
            Debug.Log("Подключение клиента " + clientId);
        }

        public virtual void OnClientDisconnected(int clientId)
        {
            Debug.Log("Отключение клиента " + clientId);
        }

        public virtual void OnReceiveMessage(int clientId, string messageString)
        {
            Debug.Log("От клиента " + clientId + " получено сообщение : \"" + messageString + "\"");
        }

        public virtual void SendMessage(string messageString)
        {
            server.SendMessageToClients(messageString);
        }

        #endregion
    }
}
