﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace WhaleDevelop.TCPServer
{
    public class Server
    {
        #region Поля

        private IPAddress ipAddress;
        private int port;
        private TcpListener tcpListener;

        // Словарь клиентов, где ключ - это id клиента, а значение - это объект с методами взаимодействия с клиентом
        private Dictionary<int, ServerClient> clients = new Dictionary<int, ServerClient>();

        private bool isRunning;

        #endregion

        #region События и геттеры

        public Action<string, int> onRun;
        public Action onClose;
        public Action<int> onClientConnected;
        public Action<int> onClientDisconnected;
        public Action<int, string> onReceiveMessage;
        public bool IsRunning => isRunning;

        #endregion

        public Server(IPAddress ipAddress, int port)
        {
            this.ipAddress = ipAddress;
            this.port = port;
        }

        #region Запуск / закрытие сервера

        public void Run()
        {
            tcpListener = new TcpListener(ipAddress, port);
            tcpListener.Start();
            isRunning = true;
            onRun?.Invoke(ipAddress.ToString(), port);
            try
            {
                ListenToClients();
            }
            catch
            {
                Close();
            }
        }

        public void Close()
        {
            tcpListener.Stop();
            isRunning = false;
            onClose?.Invoke();
        }

        #endregion


        #region Ожидание/получение сообщений от клиентов

        private async Task ListenToClients()
        {
            // Пока сервер запущен, мы ожидаем подключения новых клиентов
            while (isRunning)
            {
                // Здесь происходит остановка, пока не подключится клиент
                TcpClient tcpClient = await tcpListener.AcceptTcpClientAsync();
                // Обрабатываем подключение клиента
                HandleClientConnection(tcpClient);
                // Ждем следующего клиента
            }
        }

        /// <summary>
        /// Обработка подключения клиента - ожидаем от него входящих сообшений,
        /// заносим информацию о нем в список, чтобы можно было с ним взаимодействовать
        /// </summary>
        /// <param name="tcpClient"></param>
        /// <returns></returns>
        private async Task HandleClientConnection(TcpClient tcpClient)
        {
            int clientId = DefineClientId();
            // ServerClient - это IDisposable объект для освобождения неуправляемых ресурсов
            // при обрыве соединения
            using (ServerClient client = new ServerClient(tcpClient))
            {
                clients.Add(clientId, client);
                onClientConnected?.Invoke(clientId);

                // Пока клиент подключен, мы ждем от него сообщений
                while (tcpClient.Connected)
                {
                    // Здесь происходит остановка до тех пор, пока не придет сообщение
                    string message = await client.ListenToMessagesFromClient();
                    // После получения сообщения обрабатываем его получение 
                    onReceiveMessage?.Invoke(clientId, message);
                    // и снова возвращаемся в режим ожидания сообщения
                }
            }
        }

        private int DefineClientId()
        {
            List<int> clientsIds = clients.Keys.ToList();
            int lastAddedClientId = clientsIds.Count > 0 ? clientsIds[clientsIds.Count - 1] : 0;
            return lastAddedClientId + 1;
        }

        #endregion

        #region Отправка сообщений клиентам

        /// <summary>
        /// Отправка всем клиентам
        /// </summary>
        public void SendMessageToClients(string message)
        {
            byte[] messageBytes = GetBytesFromString(message);
            foreach (KeyValuePair<int, ServerClient> client in clients)
                client.Value.SendMessage(messageBytes);
        }

        /// <summary>
        /// Отправка всем за исключением одного
        /// </summary>
        /// <param name="message">текст рассылаемого сообщения</param>
        /// <param name="exceptionClientId">id клиента, исключенного из списка получателей</param>
        public void SendMessageToClients(string message, int exceptionClientId)
        {
            byte[] messageBytes = GetBytesFromString(message);
            foreach (KeyValuePair<int, ServerClient> client in clients)
                if (client.Key != exceptionClientId)
                    client.Value.SendMessage(messageBytes);
        }

        /// <summary>
        /// Отправка конкретным клиентам
        /// </summary>
        /// <param name="message">текст сообщения</param>
        /// <param name="recipientsIds">список id клиентов, которым нужно отправить сообщение</param>
        public void SendMessageToClients(string message, List<int> recipientsIds)
        {
            byte[] messageBytes = GetBytesFromString(message);
            foreach (KeyValuePair<int, ServerClient> client in clients)
                if (recipientsIds.Contains(client.Key))
                    client.Value.SendMessage(messageBytes);
        }

        /// <summary>
        /// Получает массив байтов сообщения, с учетом того, что туда еще
        /// нужно поместить информацию о длине сообщения
        /// </summary>
        /// <param name="messageData"></param>
        /// <returns></returns>
        public static byte[] GetBytesFromString(string messageData)
        {
            // Количество байтов на само сообщение
            byte[] dataBytes = Encoding.UTF8.GetBytes(messageData);
            byte[] lengthBytes = BitConverter.GetBytes(dataBytes.Length);
            // Склеивает в один массив байтов инфу о длине сообщения и само сообщение
            byte[] messageBytes = new byte[lengthBytes.Length + dataBytes.Length];
            for (int i = 0; i < lengthBytes.Length; i++)
                messageBytes[i] = lengthBytes[i];
            for (int i = 0; i < dataBytes.Length; i++)
                messageBytes[i + lengthBytes.Length] = dataBytes[i];
            return messageBytes;
        }
        #endregion

        #region Отключение клиента

        public void DisconnectClient(int clientId)
        {
            onClientDisconnected?.Invoke(clientId);
            clients[clientId].Dispose();            
            clients.Remove(clientId);
        }

        public void ClearClients()
        {
            if (clients != null && clients.Count > 0)
            {
                foreach (int clientId in clients.Keys)
                {
                    onClientDisconnected?.Invoke(clientId);
                    clients[clientId].Dispose();
                }
                clients.Clear();
            }
            Close();
            Run();
        }

        #endregion
    }
}